// JavaScript source code
$('#submit-recipe').click(() => {
    var foodbook = {};

    foodbook.name = $('#recipe-name').val();
    foodbook.instru = $('#recipe-instruc').val();
    foodbook.time = $('#recipe-time').val();



    if (foodbook.name != '' && foodbook.instru != '' && foodbook.time != '') {
        var recipe = JSON.parse(localStorage.getItem('foodbook'));
        if (recipe == null) {
            var recipe = [];
        }

        var file = document.getElementById('file').files[0];
        if (file == null) {
            swal("Error", "Please Enter a Image", "error");
            return;
        }
        var reader = new FileReader();
        reader.readAsDataURL(file);


        reader.onload = (e) => {
            var img = new Image();
            img.src = reader.result;
            foodbook.image = reader.result;
            recipe.push(foodbook);
            localStorage.setItem('foodbook', JSON.stringify(recipe))
            document.getElementById('recipe-name').value = '';
            document.getElementById('recipe-instruc').value = '';
            document.getElementById('recipe-time').value = '';
            $('#img-show').html('')
            swal("Success", "Your Submit Your Recipe", "success")

        }


    }
    else {
        document.getElementById('recipe-name').value = '';
        document.getElementById('recipe-instruc').value = '';
        document.getElementById('recipe-time').value = '';
        $('#img-show').html('')
        swal("Error", "Please Fill in the Boxes", "error")
    }
});

$('#file').change(() => {
    var file = document.getElementById('file').files[0];
    var fileType = /image./;
    if (file.type.match(fileType)) {
        var reader = new FileReader();
        reader.readAsDataURL(file);


        reader.onload = (e) => {
            $('#img-show').html('')
            var img = new Image();
            img.src = reader.result;
            img.width = '538'
            img.height = '178'
            $('#img-show').append(img)
        }
    }
    else {
        $('img-show').html('File not supported')
    }
});

function displayRecipe() {
    var recipe = JSON.parse(localStorage.getItem('foodbook'));
    var table = '';
    document.getElementById('hid').removeAttribute('hidden')
    if (recipe != null) {
        for (var i = 0; i < recipe.length; i++) {
            table += `<tr>\
                        <td>${recipe[i].name}</td>\
                        <td>${recipe[i].instru}</td>\
                        <td>${recipe[i].time}</td>\
                        <td><img src='${recipe[i].image}' width='64' height='64'></td>\
                        <td><button class="btn btn-danger" onclick="trash(${i})"><i class="fa fa-trash"></i></button></td>\
                    </tr>`
        }
        table += '</tbody>'
        document.getElementById('table').innerHTML = table;
    }
    else {
        document.getElementById('table').innerHTML = 'No Data Save';
    }
}
$('#new-recipe').click(() => {
    document.getElementById('recipe-name').value = '';
    document.getElementById('recipe-instruc').value = '';
    document.getElementById('recipe-time').value = '';
    $('#img-show').html('')
})

function trash(i) {
    var recipe = JSON.parse(localStorage.getItem('foodbook'));
    recipe.splice(i, 1)
    localStorage.setItem('foodbook', JSON.stringify(recipe))
    displayRecipe();
}